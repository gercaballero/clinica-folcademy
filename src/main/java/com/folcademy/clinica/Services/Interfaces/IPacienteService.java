package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Models.Dtos.PacienteDto;
import java.util.List;

public interface IPacienteService {

    List<PacienteDto> findAll();
}
