package com.folcademy.clinica.Services.Interfaces;
import com.folcademy.clinica.Models.Dtos.MedicoDto;
import com.folcademy.clinica.Models.Dtos.TurnoDto;
import com.folcademy.clinica.Models.Dtos.TurnoEnteroDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ITurnoService {
    Page<TurnoDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField);
//    List<TurnoEnteroDto> findAllTurnos();
//    List<TurnoDto> findAllTurnos();
}
