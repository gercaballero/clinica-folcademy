package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Models.Dtos.MedicoDto;
import com.folcademy.clinica.Models.Entities.Medico;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IMedicoService {
    Page<MedicoDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField);

}
