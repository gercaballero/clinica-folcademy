package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Models.Dtos.MedicoDto;
import com.folcademy.clinica.Models.Entities.Medico;
import com.folcademy.clinica.Models.Entities.Persona;
import com.folcademy.clinica.Models.Mappers.MedicoMapper;
import com.folcademy.clinica.Models.Mappers.PersonaMapper;
import com.folcademy.clinica.Models.Repositories.MedicoRepository;
import com.folcademy.clinica.Models.Repositories.PersonaRepository;
import com.folcademy.clinica.Services.Interfaces.IMedicoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service("medicoService")
public class MedicoService implements IMedicoService {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;
    private final PersonaRepository personaRepository;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper, PersonaRepository personaRepository) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
        this.personaRepository = personaRepository;
    }


    @Override
    public Page<MedicoDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return medicoRepository.findAll(pageable).map(medicoMapper::entityToDto);
    }
    public Page<MedicoDto> findMedicoById(Integer id) {
        if (!medicoRepository.existsById(id))
            throw new NotFoundException("Buscar fallido: Id " + id + " no existe");
        Pageable pageable = PageRequest.of(0, 1, Sort.by("id"));
        return medicoRepository.findById(id, pageable).map(medicoMapper::entityToDto);
    }
    public MedicoDto findOne(Integer id) {
        if(!medicoRepository.existsById(id)){
            throw new NotFoundException("No se encontro el ID");
        }
        return medicoRepository.findById(id).map(medicoMapper::entityToDto).orElse(null);
    }
    public MedicoDto newMedico(MedicoDto dto) {
        Persona persona = PersonaMapper.dtoToEntity(dto.getPersona());
        Pattern pattern = Pattern.compile("^[a-zA-Z]+$");
        Matcher nom = pattern.matcher(persona.getNombre());
        Matcher ape = pattern.matcher(persona.getApellido());
        dto.setId(null);
        if((!nom.find()) || (!ape.find()))
            throw new ValidationException("El nombre o apellido no debe tener números");
        if (dto.getConsulta() < 0)
            throw new BadRequestException("La consulta no puede ser menor a 0");
        if(dto.getProfesion() == null || dto.getProfesion() == "")
            throw new BadRequestException("Se debe ingresar una profesion");
        personaRepository.save(persona);
        return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(dto,persona)));
    }

    public boolean delete(Integer id) {
        if(!medicoRepository.existsById(id)){
            throw new NotFoundException("No se encontro el ID");
        }
        MedicoDto medico = findOne(id);
        personaRepository.deleteById(medico.getPersona().getIdpersona());
        medicoRepository.deleteById(id);
        return true;
    }

    public MedicoDto edit(Integer id, MedicoDto dto){
        Persona persona = PersonaMapper.dtoToEntity(dto.getPersona());
        Pattern pattern = Pattern.compile("^[a-zA-Z]+$");
        Matcher matcher = pattern.matcher(persona.getNombre());
        if(!medicoRepository.existsById(id)){
            throw new NotFoundException("No se encontro el ID");
        }
        if(!matcher.find())
            throw new ValidationException("El nombre no debe contener números");
        else {
            dto.setId(id);
            personaRepository.save(persona);
            return medicoMapper.entityToDto(
                    medicoRepository.save(
                            medicoMapper.dtoToEntity(dto,persona)
                    )
            );
        }
    }
}
