package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Models.Dtos.MedicoDto;
import com.folcademy.clinica.Models.Dtos.PacienteDto;
import com.folcademy.clinica.Models.Dtos.PersonaDto;
import com.folcademy.clinica.Models.Entities.Medico;
import com.folcademy.clinica.Models.Entities.Paciente;
import com.folcademy.clinica.Models.Entities.Persona;
import com.folcademy.clinica.Models.Mappers.PacienteMapper;
import com.folcademy.clinica.Models.Mappers.PersonaMapper;
import com.folcademy.clinica.Models.Repositories.PacienteRepository;
import com.folcademy.clinica.Models.Repositories.PersonaRepository;
import com.folcademy.clinica.Services.Interfaces.IPacienteService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class PacienteService implements IPacienteService {

    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;
    private final PersonaRepository personaRepository;

    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper, PersonaRepository personaRepository) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
        this.personaRepository = personaRepository;
    }


    @Override
    public List<PacienteDto> findAll() {
        List<Paciente> pacientes = (List<Paciente>) pacienteRepository.findAll();
        return pacientes.stream().map(pacienteMapper::entityToDto).collect(Collectors.toList());

//        return pacienteRepository.findAll().stream().map(pacienteMapper::entityToDto).collect(Collectors.toList());
    }
    public Page<PacienteDto> findPacienteById(Integer id) {
        if (!pacienteRepository.existsById(id))
            throw new NotFoundException("Buscar fallido: Id " + id + " no existe");
        Pageable pageable = PageRequest.of(0, 1, Sort.by("id"));
        return pacienteRepository.findById(id, pageable).map(pacienteMapper::entityToDto);
    }
    public PacienteDto findOne(Integer id) {
        if (!pacienteRepository.existsById(id)) {
            throw new NotFoundException("No se encontro el ID");
        }
        return pacienteRepository.findById(id).map(pacienteMapper::entityToDto).orElse(null);
    }

    public PacienteDto newPaciente(PacienteDto dto) {
        Persona persona = PersonaMapper.dtoToEntity(dto.getPersona());
        Pattern pattern = Pattern.compile("^[a-zA-Z]+$");
        Pattern num = Pattern.compile("^[0-9]+$");
        Matcher nom = pattern.matcher(persona.getNombre());
        Matcher ape = pattern.matcher(persona.getApellido());
        Matcher dni = num.matcher(persona.getDni());
        dto.setId(null);
        if ((persona.getDni().equals("")) || (persona.getDni().equals(null))) {
            throw new BadRequestException("El dni es obligatorio");
        }
        if (!dni.find())
            throw new ValidationException("El dni debe tener solo numeros");
        if (!nom.find() || !ape.find()) {
            throw new ValidationException("Nombre y apellido debe tener solo letras");
        }
        personaRepository.save(persona);
        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(dto, persona)));
    }

    public Boolean delete(Integer id) {
        if (!pacienteRepository.existsById(id)) {
            throw new NotFoundException("No se encontro el ID");
        }
        PacienteDto paciente = findOne(id);
        personaRepository.deleteById(paciente.getPersona().getIdpersona());
        pacienteRepository.deleteById(id);
        return true;
    }

    public PacienteDto edit(Integer id, PacienteDto dto) {
        Persona persona = PersonaMapper.dtoToEntity(dto.getPersona());
        if (!pacienteRepository.existsById(id)) {
            throw new NotFoundException("No se encontro el ID");
        }
        if (persona.getDni() == null || persona.getDni() == "")
            throw new BadRequestException("Se debe ingresar el DNI");
        else {
            dto.setId(id);
            return pacienteMapper.entityToDto(
                    pacienteRepository.save(
                            pacienteMapper.dtoToEntity(dto, persona)
                    )
            );
        }
    }
    public Page<PacienteDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return pacienteRepository.findAll(pageable).map(pacienteMapper::entityToDto);
    }
}
