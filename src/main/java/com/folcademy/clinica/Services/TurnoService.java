package com.folcademy.clinica.Services;
import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.ErrorMessage;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Models.Dtos.MedicoDto;
import com.folcademy.clinica.Models.Dtos.TurnoDto;
import com.folcademy.clinica.Models.Dtos.TurnoEnteroDto;
import com.folcademy.clinica.Models.Entities.Paciente;
import com.folcademy.clinica.Models.Entities.Turno;
import com.folcademy.clinica.Models.Mappers.TurnoMapper;
import com.folcademy.clinica.Models.Repositories.MedicoRepository;
import com.folcademy.clinica.Models.Repositories.PacienteRepository;
import com.folcademy.clinica.Models.Repositories.TurnoRepository;
import com.folcademy.clinica.Services.Interfaces.ITurnoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TurnoService implements ITurnoService {

    private final TurnoRepository turnoRepository;
    private final MedicoRepository medicoRepository;
    private final PacienteRepository pacienteRepository;
    private final TurnoMapper turnoMapper;

    public TurnoService(TurnoRepository turnoRepository, MedicoRepository medicoRepository, PacienteRepository pacienteRepository, TurnoMapper turnoMapper) {
        this.turnoRepository = turnoRepository;
        this.medicoRepository = medicoRepository;
        this.pacienteRepository = pacienteRepository;
        this.turnoMapper = turnoMapper;
    }
    @Override
    public Page<TurnoDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return turnoRepository.findAll(pageable).map(turnoMapper::entityToDto);
    }
    public Page<TurnoDto> findTurnoById(Integer id) {
        if (!turnoRepository.existsById(id))
            throw new NotFoundException("Buscar fallido: Id " + id + " no existe");
        Pageable pageable = PageRequest.of(0, 1, Sort.by("id"));
        return turnoRepository.findById(id, pageable).map(turnoMapper::entityToDto);
    }

    public  TurnoDto agregar(TurnoDto entity){
        if(medicoRepository.existsById(entity.getIdmedico()))
        { if(pacienteRepository.existsById(entity.getIdpaciente()))
        {
            entity.setId(null);
            return  turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity((entity))));
        }
        else
        {
            throw new BadRequestException(ErrorMessage.Paciente_Not_exist);}
        }
        else
        {throw new NotFoundException(ErrorMessage.Medico_Not_exist);}
    }

    public TurnoDto editar(Integer idturno,TurnoDto dto){
        if(!turnoRepository.existsById(idturno))   /* pregunta si existe el medico por id*/
            throw new NotFoundException(ErrorMessage.Turno_Not_exist);
        else { dto.setId(idturno);
            return  turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(dto)));}
    }
    public  boolean eliminar(Integer idturno){
        if(!turnoRepository.existsById(idturno))
            throw new NotFoundException(ErrorMessage.Turno_Not_exist);
        turnoRepository.deleteById(idturno);
        return true;
    }
}
