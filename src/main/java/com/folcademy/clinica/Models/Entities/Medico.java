package com.folcademy.clinica.Models.Entities;

import com.folcademy.clinica.Models.Dtos.PacienteDto;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name ="medico")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Medico{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmedico", columnDefinition = "INT(10) UNSIGNED")
    public Integer id;
    @Column(name = "Profesion", columnDefinition = "VARCHAR")
    public String profesion="";
    @Column(name = "Consulta", columnDefinition = "INT")
    public Integer consuta;
    @Column(name = "idpersona", columnDefinition = "INT")
    Integer idpersona;

    @OneToOne(cascade = {CascadeType.MERGE})
    @NotFound(action = NotFoundAction.EXCEPTION)
    @JoinColumn(name = "idpersona", referencedColumnName = "idpersona", insertable = false, updatable = false)
    Persona persona;

}
