package com.folcademy.clinica.Models.Mappers;
import com.folcademy.clinica.Models.Dtos.TurnoDto;
import com.folcademy.clinica.Models.Entities.Turno;
import com.folcademy.clinica.Models.Dtos.TurnoEnteroDto;
import org.springframework.stereotype.Component;
import java.util.Optional;
@Component
public class TurnoMapper {
    public TurnoDto entityToDto(Turno entity)
    {return Optional
            .ofNullable(entity)
            .map(ent->new TurnoDto(
                    ent.getId(),
                    ent.getFecha(),
                    ent.getHora(),
                    ent.getAtendido(),
                    ent.getIdpaciente(),
                    ent.getIdmedico()
            ))
            .orElse (new TurnoDto());
    }

    public Turno dtoToEntity(TurnoDto dto){
        Turno entity= new Turno();
        entity.setId(dto.getId());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setAtendido(dto.getAtendido());
        entity.setIdpaciente(dto.getIdpaciente());
        entity.setIdmedico(dto.getIdmedico());
        return entity;
    }

    public TurnoEnteroDto entityToEnteroDto(Turno entity) {
        TurnoEnteroDto turno = new TurnoEnteroDto();
        turno.setIdTurno(entity.getId());
        turno.setFecha(entity.getFecha());
        turno.setHora(entity.getHora());
        turno.setAtendido(entity.getAtendido());
        turno.setMedico(entity.getMedico());
        turno.setPaciente(entity.getPaciente());
        return turno;

    }

    public Turno enteroDtoToEntity(TurnoEnteroDto dto) {
        Turno entity = new Turno();
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setAtendido(dto.getAtendido());
        return entity;
    }
}
