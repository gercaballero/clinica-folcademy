package com.folcademy.clinica.Models.Mappers;
import com.folcademy.clinica.Models.Dtos.PersonaDto;
import com.folcademy.clinica.Models.Entities.Persona;
import org.springframework.stereotype.Component;
import java.util.Optional;

@Component
public class PersonaMapper {

    public static PersonaDto entityToDto(Persona entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PersonaDto(
                                ent.getId(),
                                ent.getDni(),
                                ent.getNombre(),
                                ent.getApellido(),
                                ent.getTelefono()
                        )
                )
                .orElse(new PersonaDto());
    }
    public static Persona dtoToEntity(PersonaDto dto){
        Persona entity = new Persona();
        entity.setId(dto.getIdpersona());
        entity.setDni(dto.getDni());
        entity.setNombre(dto.getNombre());
        entity.setApellido(dto.getApellido());
        entity.setTelefono(dto.getTelefono());
        return entity;
    }
}