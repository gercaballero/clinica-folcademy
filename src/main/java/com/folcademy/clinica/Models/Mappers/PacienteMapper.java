package com.folcademy.clinica.Models.Mappers;

import com.folcademy.clinica.Models.Dtos.PacienteDto;
import com.folcademy.clinica.Models.Entities.Paciente;
import com.folcademy.clinica.Models.Entities.Persona;
import org.springframework.stereotype.Component;
import java.util.Optional;

@Component
public class PacienteMapper {
    public PacienteDto entityToDto(Paciente entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteDto(
                                ent.getId(),
                                ent.getDireccion(),
                                PersonaMapper.entityToDto(ent.getPersona())
                        )
                )
                .orElse(new PacienteDto());
    }

    public Paciente dtoToEntity(PacienteDto dto, Persona persona){
        Paciente entity = new Paciente();

        entity.setId(dto.getId());
        entity.setDireccion(dto.getDireccion());
        entity.setPersona(persona);
        entity.setIdpersona(persona.getId());
        return entity;
    }
}
