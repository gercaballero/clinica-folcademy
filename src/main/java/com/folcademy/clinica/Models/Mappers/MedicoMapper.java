package com.folcademy.clinica.Models.Mappers;

import com.folcademy.clinica.Models.Dtos.MedicoDto;
import com.folcademy.clinica.Models.Dtos.PacienteDto;
import com.folcademy.clinica.Models.Entities.Medico;
import com.folcademy.clinica.Models.Entities.Persona;
import org.springframework.stereotype.Component;

import java.util.Optional;
@Component
public class MedicoMapper {

    public MedicoDto entityToDto(Medico entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoDto(
                                ent.getId(),
                                ent.getProfesion(),
                                ent.getConsuta(),
                                PersonaMapper.entityToDto(ent.getPersona())
                        )
                )
                .orElse(new MedicoDto());
    }



    public Medico dtoToEntity(MedicoDto dto, Persona persona){
        Medico entity = new Medico();
        entity.setId(dto.getId());
        entity.setProfesion(dto.getProfesion());
        entity.setConsuta(dto.getConsulta());
        entity.setPersona(persona);
        entity.setIdpersona(persona.getId());
        return entity;
    }
}
