package com.folcademy.clinica.Models.Repositories.Security;

import com.folcademy.clinica.Models.Entities.Security.AuthorizedGrantTypes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorizedGrantTypesRepository extends JpaRepository<AuthorizedGrantTypes,Integer> {
}
