package com.folcademy.clinica.Models.Repositories;

import com.folcademy.clinica.Models.Entities.Persona;
import com.folcademy.clinica.Models.Entities.Turno;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository("personaRepository")
public interface PersonaRepository extends PagingAndSortingRepository<Persona,Integer> {

}