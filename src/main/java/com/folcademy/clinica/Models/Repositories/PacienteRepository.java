package com.folcademy.clinica.Models.Repositories;

import com.folcademy.clinica.Models.Entities.Medico;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import com.folcademy.clinica.Models.Entities.Paciente;

import java.util.List;


@Repository("pacienteRepository")
public interface PacienteRepository extends PagingAndSortingRepository<Paciente, Integer> {
    Page<Paciente> findAll(Pageable pageable);
    Page<Paciente> findById(Integer id, Pageable pageable);
}
