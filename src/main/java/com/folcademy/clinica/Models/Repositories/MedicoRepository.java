package com.folcademy.clinica.Models.Repositories;

import com.folcademy.clinica.Models.Entities.Medico;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.stream.Stream;

@Repository("medicoRepository")
public interface MedicoRepository extends PagingAndSortingRepository<Medico, Integer> {
    Page<Medico> findAll(Pageable pageable);
    Page<Medico> findById(Integer id, Pageable pageable);
}

