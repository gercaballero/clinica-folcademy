package com.folcademy.clinica.Models.Dtos;
import com.folcademy.clinica.Models.Entities.Medico;
import com.folcademy.clinica.Models.Entities.Paciente;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoDto {
    public Integer id;
    public LocalDate fecha;
    public LocalTime hora;
    public Boolean atendido;
    public Integer idpaciente;
    public Integer idmedico;
}
