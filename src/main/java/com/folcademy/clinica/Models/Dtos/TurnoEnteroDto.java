package com.folcademy.clinica.Models.Dtos;

import com.folcademy.clinica.Models.Entities.Medico;
import com.folcademy.clinica.Models.Entities.Paciente;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoEnteroDto {
    Integer idTurno;

    LocalDate fecha;

    LocalTime hora;

    Boolean atendido;

    Paciente paciente;

    Medico medico;


}
