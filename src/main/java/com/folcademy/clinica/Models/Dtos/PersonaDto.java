package com.folcademy.clinica.Models.Dtos;

import com.folcademy.clinica.Models.Entities.Persona;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonaDto{
    Integer idpersona;
    @NotNull
    String dni;
    String nombre;
    String apellido;
    String telefono;
}

