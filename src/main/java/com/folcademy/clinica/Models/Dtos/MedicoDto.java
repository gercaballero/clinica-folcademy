package com.folcademy.clinica.Models.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicoDto {
    Integer id;
    String Profesion;
    Integer Consulta;
    PersonaDto persona;
}
