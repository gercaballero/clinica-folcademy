package com.folcademy.clinica.Models.Dtos;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class PacienteDto {
    Integer id;
    @NotNull
    String direccion;
    PersonaDto persona;
}

