package com.folcademy.clinica.Exceptions;
public class ErrorMessage {
    private  String messege;
    private String detall;
    private String code;
    private String path;

    public ErrorMessage(String messege, String detall, String code, String path) {
        this.messege = messege;
        this.detall = detall;
        this.code = code;
        this.path = path;
    }

    public String getMessege() {
        return messege;
    }
    public String getDetall() {
        return detall;
    }
    public String getCode() {
        return code;
    }
    public String getPath() {
        return path;
    }
    public static final String Paciente_Not_exist="El paciente no existe ";
    public static final String Medico_Not_exist="El Medico no existe ";
    public static final String Turno_Not_exist="El Turno no existe";

}
