package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Models.Dtos.TurnoDto;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/turno")
public class TurnoController {
    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

    @PreAuthorize("hasAuthority('get_turnos')")
    @GetMapping(value ="")
    public ResponseEntity<Page<TurnoDto>> listarTodoByPage(
            @RequestParam(name= "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name= "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name= "orderField", defaultValue = "fecha") String orderField
    ) {
        return  ResponseEntity.ok(turnoService.listarTodosByPage(pageNumber,pageSize,orderField));
    }



    @PreAuthorize("hasAuthority('get_one')")
    @GetMapping("/{id}")
    public  ResponseEntity<Page<TurnoDto>>listarUno(@PathVariable(name="id")Integer id){
        return  ResponseEntity.ok(turnoService.findTurnoById(id));
    }


    @PreAuthorize("hasAuthority('insertar_turnos')")
    @PostMapping(value = "/new")
    public ResponseEntity<TurnoDto> agregar(@RequestBody @Validated TurnoDto dto){
        return ResponseEntity
                .ok()
                .body(
                        turnoService.agregar(dto)
                );
    }

    @PreAuthorize("hasAuthority('editar')")
    @PutMapping("/edit/{idturno}")
    public ResponseEntity<TurnoDto>editar(@PathVariable(name="idturno")int id,@RequestBody TurnoDto dto) {
        return ResponseEntity.ok(turnoService.editar(id,dto));
    }

    @PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{idturno}")
    public  ResponseEntity<Boolean>eliminar(@PathVariable(name="idturno")int id){
        return  ResponseEntity.ok(turnoService.eliminar(id));
    }
}
