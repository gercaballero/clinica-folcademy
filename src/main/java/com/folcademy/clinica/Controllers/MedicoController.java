package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Models.Dtos.MedicoDto;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/medicos")
public class MedicoController {

    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }


    @PreAuthorize("hasAuthority('get_medicos')")
    @GetMapping(value ="")
    public ResponseEntity<Page<MedicoDto>> listarTodoByPage(
            @RequestParam(name= "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name= "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name= "orderField", defaultValue = "persona.apellido") String orderField
    ) {
        return  ResponseEntity.ok(medicoService.listarTodosByPage(pageNumber,pageSize,orderField));
    }
    @PreAuthorize("hasAuthority('get_one')")
    @GetMapping(value ="/{id}")
    public  ResponseEntity<Page<MedicoDto>>listarTodoById(@PathVariable(name="id")Integer id)
    {
        return  ResponseEntity.ok(medicoService.findMedicoById(id));
    }
    @PreAuthorize("hasAuthority('insertar')")
    @PostMapping(value = "/new")
    public ResponseEntity<MedicoDto> saveMedico(@RequestBody @Validated MedicoDto entity){
        return ResponseEntity
                .ok()
                .body(
                        medicoService.newMedico(entity)
                )
        ;
    }
    @PreAuthorize("hasAuthority('delete')")
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<?> deleteOne(@PathVariable(name="id") Integer id){
        if(medicoService.findOne(id) != null){
            medicoService.delete(id);
            return ResponseEntity.ok().build();
        }
        else{
            return  ResponseEntity.notFound().build();
        }

    }
    @PreAuthorize("hasAuthority('editar')")
    @PutMapping(value = "/edit/{id}")
    public ResponseEntity<?> editOne(@PathVariable(name="id") Integer id,@RequestBody MedicoDto dto){
        if(medicoService.findOne(id) != null){
            return ResponseEntity.ok().body(medicoService.edit(id,dto));
        }
        else{
            return  ResponseEntity.notFound().build();
        }
    }
}
