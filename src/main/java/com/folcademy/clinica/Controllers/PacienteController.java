package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Models.Dtos.PacienteDto;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pacientes")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }

    @PreAuthorize("hasAuthority('get_pacientes')")
    @GetMapping(value ="")
    public ResponseEntity<Page<PacienteDto>> listarTodoByPage(
            @RequestParam(name= "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name= "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name= "orderField", defaultValue = "persona.apellido") String orderField
    ) {
        return  ResponseEntity.ok(pacienteService.listarTodosByPage(pageNumber,pageSize,orderField));
    }

    @PreAuthorize("hasAuthority('get_one')")
    @GetMapping(value ="/{id}")
    public ResponseEntity<Page<PacienteDto>> findOne(@PathVariable(name="id") Integer id){
        return ResponseEntity
                .ok()
                .body(
                        pacienteService.findPacienteById(id)
                )
                ;

    }
    @PreAuthorize("hasAuthority('insertar')")
    @PostMapping(value = "/new")
    public ResponseEntity<PacienteDto> saveMedico(@RequestBody @Validated PacienteDto entity){
        return ResponseEntity
                .ok()
                .body(
                        pacienteService.newPaciente(entity)
                )
                ;
    }


    @PreAuthorize("hasAuthority('delete')")
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<?> deleteOne(@PathVariable(name="id") Integer id){
        if(pacienteService.findOne(id) != null){
            pacienteService.delete(id);
            return ResponseEntity.ok().build();
        }
        else{
            return  ResponseEntity.notFound().build();
        }

    }
    @PreAuthorize("hasAuthority('editar')")
    @PutMapping(value = "/edit/{id}")
    public ResponseEntity<?> editOne(@PathVariable(name="id") Integer id,@RequestBody PacienteDto dto){
        if(pacienteService.findOne(id) != null){
            return ResponseEntity.ok().body(pacienteService.edit(id,dto));
        }
        else{
            return  ResponseEntity.notFound().build();
        }

    }

}
